// stream-like wave_data api
#include <algorithm>
#include <memory>
#include <string>
#include <thread>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <glm/vec3.hpp>
#include <vorbis/vorbisfile.h>
#include <AL/al.h>
#include <AL/alc.h>

char const * sound_path = "../assets/test_clip.ogg";
//char const * sound_path = "fire01.ogg";

using std::vector;
using std::cout;
using std::min;
using std::shared_ptr;

/*! \note zdielat data napriec zdrojmi zvuku je mozne iba v pripade implementacie read() citajucej data s urcenej pozicie */
class wave_data
{
public:
	virtual ~wave_data() {}
	virtual uint8_t * data() = 0;
	virtual size_t stream_data(size_t size) = 0;  //!< precita size bajtov zo vstupu
	virtual size_t sample_rate() const = 0;
	virtual size_t channels() const = 0;
	virtual size_t bytes_per_sample() const = 0;
	virtual size_t sample_size() const {return bytes_per_sample() * channels();}
	virtual void reset() = 0;  //!< umozni znova citat data
};

/*! Implementuje citanie vorbisu zo suboru.
Objekt typu vorbis_data je nezdielatelny napriec zdrojmi zvuku.
\note bez fungujucej implementacie read_at() nie mozne zdielat wave data medzi zdrojmi zvuku */
class vorbis_data : public wave_data
{
public:
	vorbis_data(char const * file_name);
	vorbis_data(std::string const & file_name) : vorbis_data{file_name.c_str()} {}
	~vorbis_data();

	// wave_data api
	uint8_t * data() override;
	size_t stream_data(size_t size) override;
	size_t sample_rate() const override {return _vi->rate;}
	size_t channels() const override {return _vi->channels;}
	size_t bytes_per_sample() const override {return 2;}
	size_t sample_size() const override {return _sample_size;}
	void reset() override;  // umozni znova citat data

	size_t sample_count() const {return ov_pcm_total(const_cast<OggVorbis_File *>(&_vf), -1);}

private:
	FILE * _fin;
	OggVorbis_File _vf;
	vorbis_info * _vi;
	size_t _sample_size;
	std::vector<uint8_t> _data;
};


class blob
{
public:
	blob(vorbis_data & wave);  // TODO: nech je toto externa funkcia
	uint8_t * data(size_t offset);
	size_t size() const {return _data.size();}

private:
	vector<uint8_t> _data;
};

uint8_t * blob::data(size_t offset)
{
	return _data.data() + offset;
}

blob::blob(vorbis_data & wave)
{
	size_t const size = 4096 * wave.sample_size();

	while (1)
	{
		size_t off = _data.size();
		_data.resize(_data.size() + size);

		size_t read_bytes = wave.stream_data(size);
		if (read_bytes == 0)
			break;  // eof
		memcpy(_data.data() + off, wave.data(), read_bytes);

		if (read_bytes < size)
			_data.resize(off + read_bytes);
	}
}


class blob_data : public wave_data
{
public:
	blob_data(std::shared_ptr<blob> samples, size_t sample_rate, size_t channels, size_t bytes_per_sample);
	uint8_t * data() override;
	size_t stream_data(size_t size) override;
	size_t sample_rate() const override;
	size_t channels() const override;
	size_t bytes_per_sample() const override;
	void reset() override;

	size_t sample_count() const {return _samples->size();}

private:
	std::shared_ptr<blob> _samples;
	size_t _rate;
	size_t _channels;
	size_t _bytes_per_sample;
	size_t _sample_size;
	size_t _offset;
};

blob_data::blob_data(std::shared_ptr<blob> samples, size_t sample_rate, size_t channels, size_t bytes_per_sample)
	: _samples{samples}
	, _rate{sample_rate}
	, _channels{channels}
	, _bytes_per_sample{bytes_per_sample}
	, _sample_size{_channels * bytes_per_sample}
	, _offset{0}
{}

uint8_t * blob_data::data()
{
	return _samples->data(_offset);
}

size_t blob_data::stream_data(size_t size)
{
	size_t read_bytes = min(_samples->size() - _offset, size);
	_offset += read_bytes;
	return read_bytes;
}

size_t blob_data::sample_rate() const
{
	return _rate;
}

size_t blob_data::channels() const
{
	return _channels;
}

size_t blob_data::bytes_per_sample() const
{
	return _bytes_per_sample;
}

void blob_data::reset()
{
	_offset = 0;
}


class audio_source
{
public:
	audio_source();
	~audio_source();
	void play();
	void attach(wave_data * wave);
	void update();
	void stop();
	bool playing() const;
	bool paused() const;
	void position(glm::vec3 const & p);
	void velocity(glm::vec3 const & v);
	void direction(glm::vec3 const & d);

private:
	static constexpr int NUM_BUFFERS = 4;
	static constexpr int BUFFER_TIME_MS = 200;

	void unqueue_buffers_all();
	int unqueue_buffers(ALuint unqueued[NUM_BUFFERS]);
	size_t fill_buffers(size_t n, ALuint * buffers);
	void free();

	ALuint _source;
	ALuint _buffers[NUM_BUFFERS];
	wave_data * _wave;  //!< audio_source vlastni wave data
	size_t _buffer_size;  //!< in bytes
	ALenum _format;
};

audio_source::audio_source()
{
	alGenSources(1, &_source);
	_buffer_size = 0;
	_wave = nullptr;
}

void audio_source::play()
{
	assert(_wave && "not data attached");

	stop();
	unqueue_buffers_all();
	_wave->reset();

	size_t filled = fill_buffers(NUM_BUFFERS, _buffers);
	assert(filled > 0 && "nothing buffered");

	alSourceQueueBuffers(_source, filled, _buffers);
	alSourcePlay(_source);

	assert(alGetError() == AL_NO_ERROR && "openal: source queue failed");
}

void audio_source::update()
{
	if (!playing())
		return;

	assert(_wave && "wave data not initialized");

	ALuint unqueued[NUM_BUFFERS];
	int unqueue_count = unqueue_buffers(unqueued);
	if (unqueue_count == 0)
		return;

	size_t refilled = fill_buffers(unqueue_count, unqueued);

	alSourceQueueBuffers(_source, refilled, unqueued);
}

void audio_source::attach(wave_data * wave)
{
	free();
	_wave = wave;

	// buffers
	alGenBuffers(NUM_BUFFERS, _buffers);

	if (_wave->channels() > 1)
		_format = (_wave->bytes_per_sample() == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_STEREO8);
	else
		_format = (_wave->bytes_per_sample() == 2 ? AL_FORMAT_MONO16 : AL_FORMAT_MONO8);

	_buffer_size = (BUFFER_TIME_MS * _wave->sample_rate() * _wave->sample_size()) / 1000;
}

audio_source::~audio_source()
{
	free();
	alDeleteSources(1, &_source);
	delete _wave;
}

void audio_source::stop()
{
	alSourceStop(_source);
}

bool audio_source::playing() const
{
	int state;
	alGetSourcei(_source, AL_SOURCE_STATE, &state);
	return state == AL_PLAYING;
}

bool audio_source::paused() const
{
	int state;
	alGetSourcei(_source, AL_SOURCE_STATE, &state);
	return state == AL_PAUSED;
}

void audio_source::position(glm::vec3 const & p)
{
	alSource3f(_source, AL_POSITION, p.x, p.y, p.z);
}

void audio_source::velocity(glm::vec3 const & v)
{
	alSource3f(_source, AL_VELOCITY, v.x, v.y, v.z);
}

void audio_source::direction(glm::vec3 const & d)
{
	alSource3f(_source, AL_DIRECTION, d.x, d.y, d.z);
}

void audio_source::unqueue_buffers_all()
{
	stop();
	ALuint dummy[NUM_BUFFERS];
	unqueue_buffers(dummy);
}

int audio_source::unqueue_buffers(ALuint unqueued[NUM_BUFFERS])
{
	int processed;
	alGetSourcei(_source, AL_BUFFERS_PROCESSED, &processed);
	if (processed == 0)
		return 0;

	alSourceUnqueueBuffers(_source, processed, unqueued);
	assert(alGetError() == AL_NO_ERROR && "openal: unqueue buffers failed");

	return processed;
}

void audio_source::free()
{
	if (_wave)
	{
		unqueue_buffers_all();
		alDeleteBuffers(NUM_BUFFERS, _buffers);
		_buffer_size = 0;
	}

	assert(alGetError() == AL_NO_ERROR && "openal: release resource failed");
}

size_t audio_source::fill_buffers(size_t n, ALuint * buffers)
{
	for (size_t i = 0; i < n; ++i)
	{
		size_t read_bytes = _wave->stream_data(_buffer_size);
		if (read_bytes == 0)
			return i;  // eof
		alBufferData(buffers[i], _format, (ALvoid *)_wave->data(), read_bytes, _wave->sample_rate());

		assert(alGetError() == AL_NO_ERROR && "openal: filling buffer failed");
	}
	return n;
}

uint8_t * vorbis_data::data()
{
	return _data.data();
}

size_t vorbis_data::stream_data(size_t size)
{
	_data.resize(size);
	int current_section = 0;
	size_t total_read_bytes = 0;
	while (size > 0)
	{
		long read_bytes = ov_read(&_vf, (char *)(_data.data() + total_read_bytes), size, 0 /*little endian*/, bytes_per_sample(), 1, &current_section);
		assert(read_bytes >= 0 && "corrupt bitstream section");
		if (read_bytes == 0)
			break;  // eof
		size -= read_bytes;
		total_read_bytes += read_bytes;
	}
	return total_read_bytes;
}

void vorbis_data::reset()
{
	int res = ov_pcm_seek(&_vf, 0);
	assert(res == 0 && "seek failed");
}

vorbis_data::vorbis_data(char const * file_name)
{
	_fin = fopen(file_name, "r");
	if (!_fin)
		throw std::runtime_error{"unable to open an input file"};

	int res = ov_open_callbacks(_fin, &_vf, NULL, 0, OV_CALLBACKS_NOCLOSE);
	assert(res == 0 && "ov_open_callbacks() failed");

	_vi = ov_info(&_vf, -1);
	assert(_vi && "ov_info() failed");

	_sample_size = bytes_per_sample() * _vi->channels;
}

vorbis_data::~vorbis_data()
{
	ov_clear(&_vf);
	fclose(_fin);
}


int main(int argc, char * argv[])
{
	// device & context
	ALCdevice * dev = alcOpenDevice(nullptr);  // open default device
	assert(dev && "failed to open a device");

	ALCcontext * ctx = alcCreateContext(dev, nullptr);
	assert(ctx && "failed to create an audio context");
	alcMakeContextCurrent(ctx);
	assert(alGetError() == AL_NO_ERROR && "context stuff failed");

	// input
	char const * fname = (argc > 1) ? argv[1] : sound_path;

	cout << fname << " open\n";

	{
		vorbis_data wave{fname};
		shared_ptr<blob> benny_blob{new blob{wave}};

		audio_source source;
		source.attach(new blob_data{benny_blob, wave.sample_rate(), wave.channels(), wave.bytes_per_sample()});
//		source.attach(new vorbis_data{fname});
		source.play();

		cout << "playing ..." << std::endl;

		size_t t = 0;

		do  // loop
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
			source.update();

			t += 20;
			if (t == 3000)
				source.play();
		}
		while (source.playing());
	}

	alcDestroyContext(ctx);
	alcCloseDevice(dev);

	cout << "done.\n";
	return 0;
}
