// test struktury vorbis_wave
#include <thread>
#include <iostream>
#include <AL/al.h>
#include <AL/alc.h>
#include "waveform.hpp"

char const * sound_path = "../assets/test_clip.wav";

using std::cout;


int main(int argc, char * argv[])
{
	// device & context
	ALCdevice * dev = alcOpenDevice(nullptr);  // open default device
	assert(dev && "failed to open a device");

	ALCcontext * ctx = alcCreateContext(dev, nullptr);
	assert(ctx && "failed to create an audio context");
	alcMakeContextCurrent(ctx);
	assert(alGetError() == AL_NO_ERROR && "context stuff failed");

	// input
	char const * fname = (argc > 1) ? argv[1] : sound_path;

	cout << fname << " open\n";

	{
		audio_source source;
		source.attach(new waveform_wave{fname});
		source.play();

		cout << "playing ..." << std::endl;

		size_t t = 0;

		do  // loop
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
			source.update();

			t += 20;
			if (t == 3000)
				source.play();
		}
		while (source.playing());
	}

	alcDestroyContext(ctx);
	alcCloseDevice(dev);

	cout << "done.\n";
	return 0;
}
