#include <thread>
#include <memory>
#include <iostream>
#include <cassert>
#include <AL/alc.h>
#include "vorbis.hpp"
#include "blob_wave.hpp"

char const * sound_path = "../assets/test_clip.ogg";

using std::shared_ptr;
using std::cout;


int main(int argc, char * argv[])
{
	// device & context
	ALCdevice * dev = alcOpenDevice(nullptr);  // open default device
	assert(dev && "failed to open a device");

	ALCcontext * ctx = alcCreateContext(dev, nullptr);
	assert(ctx && "failed to create an audio context");
	alcMakeContextCurrent(ctx);
	assert(alGetError() == AL_NO_ERROR && "context stuff failed");

	// input
	char const * fname = (argc > 1) ? argv[1] : sound_path;

	cout << fname << " open\n";

	{
		vorbis_wave wave{fname};
		shared_ptr<blob> benny_blob{new blob{wave}};

		audio_source source;
		source.attach(new blob_wave{benny_blob, wave.sample_rate(), wave.channels(), wave.bytes_per_sample()});
		source.play();

		cout << "playing ..." << std::endl;

		size_t t = 0;

		do  // loop
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
			source.update();

			t += 20;
			if (t == 3000)
				source.play();
		}
		while (source.playing());
	}

	alcDestroyContext(ctx);
	alcCloseDevice(dev);

	cout << "done.\n";
	return 0;
}

