// unifikacia citania zo zdroja
#include <thread>
#include <iostream>
#include <cassert>
#include <AL/alc.h>
#include "al/audio.hpp"
#include "al/vorbis.hpp"

char const * sound_path = "../../assets/test_clip.ogg";

using std::cout;


int main(int argc, char * argv[])
{
	// device & context
	ALCdevice * dev = alcOpenDevice(nullptr);  // open default device
	assert(dev && "failed to open a device");
	
	ALCcontext * ctx = alcCreateContext(dev, nullptr);
	assert(ctx && "failed to create an audio context");
	alcMakeContextCurrent(ctx);
	assert(alGetError() == AL_NO_ERROR && "context stuff failed");
	
	// input
	char const * fname = (argc > 1) ? argv[1] : sound_path;
	cout << fname << " open\n";
	
	{
		audio_source source;
		source.attach(std::shared_ptr<vorbis_wave>{new vorbis_wave{fname}});
		source.play();

		cout << "playing ..." << std::endl;

		source.join();
	}
	
	alcDestroyContext(ctx);
	alcCloseDevice(dev);
	
	cout << "done.\n";
	return 0;
}
