// unifikacia citania zo zdroja (stream play)
#include <thread>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <AL/alc.h>
#include "al/audio.hpp"
#include "al/vorbis.hpp"

char const * benny_sound_path = "../assets/test_clip.ogg";
char const * fire_sound_path = "../assets/fire01.ogg";

using std::cout;
using std::shared_ptr;


int main(int argc, char * argv[])
{
	// device & context
	ALCdevice * dev = alcOpenDevice(nullptr);  // open default device
	assert(dev && "failed to open a device");
	
	ALCcontext * ctx = alcCreateContext(dev, nullptr);
	assert(ctx && "failed to create an audio context");
	alcMakeContextCurrent(ctx);
	assert(alGetError() == AL_NO_ERROR && "context stuff failed");
	
	cout << benny_sound_path << " and " << fire_sound_path << " open\n";
	
	{
		audio_source benny_source, fire_source;
		benny_source.attach(shared_ptr<vorbis_wave>{new vorbis_wave{benny_sound_path}});
		fire_source.attach(shared_ptr<vorbis_wave>{new vorbis_wave{fire_sound_path}});

		benny_source.play();

		cout << "playing ..." << std::endl;

		size_t elapsed = 0;
		do  // loop
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
			elapsed += 20;

			if (elapsed > 2000)
			{
				fire_source.play();
				elapsed = 0;
			}

			benny_source.update();
			fire_source.update();
		}
		while (benny_source.playing());
	}
	
	alcDestroyContext(ctx);
	alcCloseDevice(dev);
	
	cout << "done.\n";
	return 0;
}
